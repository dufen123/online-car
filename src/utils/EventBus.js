/*
 * @FilePath: EventBus.js
 * @Author: 杜芬
 * @Date: 2024-04-20 18:48:32
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-04-21 10:21:39
 * Copyright: 2024 xxxTech CO.,LTD. All Rights Reserved.
 * @Descripttion: 页面
 */
// 集中事件总线类

export class EventBus{
constructor() {
  this.eventObj = {} // 保存事件名和要触发的函数体
}
  // 单线
  static getInstance() {
    if(!this.instance) {
      this.instance = new EventBus()
    }
    return this.instance
  }
  // 订阅事件
    on(eventName,fn) {
        if(!this.eventObj[eventName]){
          // 如果这个事件名称没有注册过，那就先声明次属性（事件名称），赋予一个回调函数的数组
          this.eventObj[eventName] = []
        }
        this.eventObj[eventName].push(fn)
    }
  // 触发事件
  //...arg:接受剩余参数
  emit(eventName,...arg) {
    // arg 次变量是一个数组（值就是按照先后顺序传入的实参）
    this.eventObj[eventName].forEach(fn => {
      fn(...arg) // 展开参数数组，按照顺序一个个传递给回调函数
    })

  }
}