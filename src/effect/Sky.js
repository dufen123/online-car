/*
 * @FilePath: Sky.js
 * @Author: 杜芬
 * @Date: 2024-04-20 11:54:17
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-04-22 14:56:58
 * Copyright: 2024 xxxTech CO.,LTD. All Rights Reserved.
 * @Descripttion: 页面
 */
// 天空和地面管理类


import * as THREE from 'three'

import { EventBus } from '@/utils/EventBus'
export class Sky {
      constructor(scene) {
        this.scene = scene
        this.nowMesh = [] // 当前背景物体对象列表
        this.skyName = '展厅' // 默认当前的位置是室内



        this.init()
      }
      //  初始化天空
      init() {
        // 默认先创建室内展示的背景环境
        this.createInDoor()
        

        // 订阅切换场景事件
        // 订阅切换场景事件
          EventBus.getInstance().on('changeSky', skyName => {
            if (skyName === this.nowSkyName) return // 防止用户反复点击同一个场景创建无用的东西
            this.clear() // 清除当前物体
            if (skyName === '展厅') {
              this.createInDoor()
              this.nowSkyName = '展厅'
            } else if (skyName === '户外') {
              this.createOutDoor()
              this.nowSkyName = '户外'
            }
          })
       
      }

      // 室内展示的背景环境
      createInDoor() {
        // 创建球体
        const sphereGeo = new THREE.SphereGeometry(10, 32, 16)
        // 材质
        const material = new THREE.MeshBasicMaterial({color:0x42454c,side:THREE.DoubleSide})
        // 物体
        const sphere = new THREE.Mesh(sphereGeo, material)
        // 加入到场景中
        this.scene.add(sphere)
        // 加入到数组中
        this.nowMesh.push(sphere)

      // 地面
      const planeGeo = new THREE.CircleGeometry(10, 32)
      const standardMaterial = new THREE.MeshStandardMaterial({ color: 0x42454c, side: THREE.DoubleSide })
      const plane = new THREE.Mesh(planeGeo, standardMaterial)
      plane.rotation.set(- Math.PI / 2, 0, 0)
      plane.receiveShadow = true // 添加阴影
      this.scene.add(plane)
      this.nowMesh.push(plane)
      }
     // 户外
     // 户外
  createOutDoor() {
    // 让球体大一些
    // 球体
    const sphereGeo = new THREE.SphereGeometry(40, 32, 16)
    const sphereTexture = (new THREE.TextureLoader()).load('image/desert.jpg') // 户外 720 度全景图片-得到纹理对象
    sphereTexture.colorSpace = THREE.SRGBColorSpace
    const material = new THREE.MeshBasicMaterial({ map: sphereTexture, side: THREE.DoubleSide })
    const sphere = new THREE.Mesh(sphereGeo, material)
    this.scene.add(sphere)
    this.nowMesh.push(sphere)

    // 地面
    const planeGeo = new THREE.CircleGeometry(20, 32)
    const planeTexture = (new THREE.TextureLoader()).load('image/sand.jpg') // 地面纹理对象
    const standardMaterial = new THREE.MeshStandardMaterial({ map: planeTexture, color: 0xa0825a, side: THREE.DoubleSide }) // 颜色和颜色贴图可以混合计算
    const plane = new THREE.Mesh(planeGeo, standardMaterial)
    plane.rotation.set(- Math.PI / 2, 0, 0)
    this.scene.add(plane)
    this.nowMesh.push(plane)
  }
 
      // 清除球体和对象
      clear() {
         this.nowMesh.forEach(obj => {
           obj.geometry.dispose() // 释放几何图形
           obj.material.dispose() // 释放材质
           obj.material.map && obj.material.map.dispose() // 纹理对象释放内存

           obj.parent.remove(obj)
         })

         this.nowMesh.splice(0,this.nowMesh.length) // 从第0个位置清空数组
      }
  }


