/*
 * @FilePath: loadManager.js
 * @Author: 杜芬
 * @Date: 2024-04-19 13:29:55
 * @LastEditors: 
 * @LastEditTime: 2024-04-20 11:22:03
 * Copyright: 2024 xxxTech CO.,LTD. All Rights Reserved.
 * @Descripttion: 页面
 */
/**
 * 
 * 
 * 专门加载模型文件的
 * @param { * } path
 * @param {  * } successFn
 * @param {  * } process 进度
 * @param {  * } error 错误结果
 */

 import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';

export function loadManager(path, successFn) {
 const gltfLoader = new GLTFLoader()
 gltfLoader.load(path,gltf => successFn(gltf.scene),process => {
  //  console.log(process)
  
  },
   error => {throw new Error(error)})
}