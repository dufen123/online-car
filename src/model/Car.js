/*
 * @FilePath: Car.js
 * @Author: 杜芬
 * @Date: 2024-04-19 13:49:33
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-04-22 16:38:02
 * Copyright: 2024 xxxTech CO.,LTD. All Rights Reserved.
 * @Descripttion: 页面
 */

import * as THREE from 'three'
import { MySprite } from '@/model/MySprite.js'
import { ClickHandler } from '@/utils/ClickHandler'
import gsap from 'gsap' // 动画

import { EventBus } from '@/utils/EventBus'
// 
// 汽车类
export class Car {
  constructor(model,scene,camera, controls) {
    this.model = model
    this.scene = scene
    this.camera = camera
    this.controls = controls


    // 汽车各种视角坐标对象
    this.positionObj = {
      // 主驾驶
      main: {
        camera: {
          x: 0.36,
          y: 0.96,
          z: -0.16
        },
        controls: {
          x: 0.36,
          y: 0.87,
          z: 0.03
        }
      },
      // 副驾驶位
      copilot: {
        camera: {
          x: -0.39,
          y: 0.87,
          z: 0.07
        },
        controls: {
          x: -0.39,
          y: 0.85,
          z: 0.13
        }
      },
      // 外面观察
      outside: {
        camera: {
          x: 3,
          y: 1.5,
          z: 3
        },
        controls: {
          x: 0,
          y: 0,
          z: 0
        }
      }
    }

     // 车（小模型对象）
  this.carModel= {
    'body' : {
      'main':{ // 车身
        name:'Object_103',
        model:{}
      },
      'roof':{ // 车顶
        name:'Object_110',
        model:{}
      },
      'leftDoor':{ // 左车门
        name:'Object_64',
        model:{},
        mark: [
          {
            name: 'sprite',
            url: 'image/sprite.png',
            scale: [0.2, 0.2],
            position: [1.07, 1.94, -0.23] // 参考车门的原点相对位移
          }
        ]

      },
      'rightDoor':{ // 右车门
        name:'Object_77',
        model:{},
        mark: [
          {
            name: 'sprite',
            url: 'image/sprite.png',
            scale: [0.2, 0.2],
            position: [-1.05, 0.78, -0.23]
          }
        ]
      }
    },
    'glass': { // 玻璃
      'front': { // 前玻璃
        name: 'Object_90',
        model: {}
      },
      'leftGlass': { // 左玻璃
        name: 'Object_68',
        model: {}
      },
      'rightGlass': { // 右玻璃
        name: 'Object_81',
        model: {}
      }
    }
  }
    // 车数值相关（记录用于发给后台-保存用户要购车相关信息）
    this.info = {
      allPrice:2444700, // 汽车默认总价

      color: [
        {
          name: '土豪金',
          color: '#ff9900',
          isSelected: true
        },
        {
          name: '传奇黑',
          color: '#343a40',
          isSelected: false
        },
        {
          name: '海蓝',
          color: '#409EFF',
          isSelected: false
        },
        {
          name: '玫瑰紫',
          color: '#6600ff',
          isSelected: false
        },
        {
          name: '银灰色',
          color: '#DCDFE6',
          isSelected: false
        }
      ],
       // 贴膜
       film: [
        {
          name: '高光',
          price: 0,
          isSelected: true
        },
        {
          name: '磨砂',
          price: 20000,
          isSelected: false
        }
      ]
    }



    this.init() // 初始化
    this.modifCardefault()
    this.createDooeSprite()
  }
 
  
  init() {
    this.scene.add(this.model) // 模型对象加入到场景中

    // 每个小物体产生阴影
    this.model.traverse(obj => obj.castShadow = true) 


    // 通过数据结构名字 =》 小物体对象保存到数据结构中
    Object.values(this.carModel.body).forEach(obj => {
      // 通过名称找到小物体
      obj.model = this.model.getObjectByName(obj.name)
    })


    // 与玻璃相关的  
    Object.values(this.carModel.glass).forEach(obj => {
      // 通过名称找到小物体
      obj.model = this.model.getObjectByName(obj.name)
    })

    //  订阅汽车修改颜色的事件和函数体
    EventBus.getInstance().on('changeCarColor',(colorStr) => {
      // console.log(colorStr)
      Object.values(this.carModel.body).forEach(obj => {
        // 通过名称找到小物体->把颜色赋值给他
        obj.model.material.color = new THREE.Color(colorStr)
      })
      
      // 保存用户选择的车颜色
      this.info.color.forEach(obj => {
        obj.isSelected = false
        if(obj.color === colorStr) {
          obj.isSelected = true
        }
      })
      // console.log( this.info.color)


    })
    
    // 订阅汽车贴膜的事件和函数体
    EventBus.getInstance().on('changeCarCoat', coatName => {
        // console.log(coatName)
        if(coatName==='高光') {
          Object.values(this.carModel.body).forEach(obj => {
            // 通过名称找到小物体->把颜色赋值给他
            // roughness 高光0.5
            // metalness 金属度 1
            // clearcoat 膜 1
            obj.model.material.roughness = 0.5
            obj.model.material.metalness = 1
            obj.model.material.clearcoat = 1
          })
        } else if (coatName === '磨砂') {
          Object.values(this.carModel.body).forEach(obj => {
            obj.model.material.roughness = 1
            obj.model.material.metalness = 0.5 // 如果为 0 显得很假
            obj.model.material.clearcoat = 0
          })
        }

        // 保存用户的贴膜类型
        Object.values(this.info.film).forEach(obj => {
          obj.isSelected = false
          if(obj.name === coatName) {
            obj.isSelected = true

          }
         
        })
    })
    
    // 、 // 为后面计算价格做准备
    EventBus.getInstance().on('celPrice', () => {
      const filmTarget = this.info.film.find(obj => obj.isSelected)

      // 动态总价
      const celPrice = this.info.allPrice + filmTarget.price

      document.querySelector('.price>span').innerHTML = `￥ ${celPrice.toFixed(2)}`

    })

    // 订阅视角切换事件
    // 订阅视角切换事件

    EventBus.getInstance().on('changeCarAngleView',viewName => {
        this.setCameraAnimation(this.positionObj[viewName])
    })


  }


    // 修改车身默认细节
    modifCardefault(){
     const bodyMaterial = new THREE.MeshPhysicalMaterial({
        color: 0xff9900,
        roughness: 0.5, // 粗糙程度0.5 半粗糙
        metalness: 1, // 金属度1
        clearcoat: 1, // 清漆度
        clearcoatRoughness: 0 // 清漆度的粗糙对 为0 不粗糙
     })
    //  赋予给每个小物体(网格材质)
    Object.values(this.carModel.body).forEach(obj => {
      obj.model.material = bodyMaterial
    })

    // 改变玻璃渲染面
    Object.values(this.carModel.glass).forEach(obj => {
      obj.model.material.side = THREE.FrontSide // 前面渲染

    })
    // 
    this.carModel.body.roof.model.material.side = THREE.DoubleSide

    }


    // 创建车门上的精灵物体
    createDooeSprite() {
        // 自定义合成数组（只有创建精灵物体对象）
    const markList = [this.carModel.body.leftDoor, this.carModel.body.rightDoor]
    // 遍历创建精灵物体
    markList.forEach(obj => {
      // 给车门所有热点标记遍历一个个生成附加的物体
      obj.mark.forEach(smallObj => {
        if (smallObj.name === 'sprite') {
          // 为数据对象生成精灵物体，并添加到小物体模型上
          const sprite = new MySprite(smallObj)
          obj.model.add(sprite)
          //  为精灵物体进行射线交互的绑定
          ClickHandler.getInstance().addMesh(sprite, (clickThreeObj) => {
            // console.log(clickThreeObj.parent) // 父级物体对象
            // clickThreeObj: 精灵物体
            // clickThreeObj.parent :是object_77 车门物体（坐标轴原点在世界坐标系中心，旋转车门有问题）
             // clickThreeObj.parent.parent.parent  （才是整个车门的最大物体对象，坐标系 在车门框点-旋转） 
            const targetDoor = clickThreeObj.parent.parent.parent  
            if(!targetDoor.userData.isOpen) {
              // 没开门状态 =》开门
              // targetDoor.rotation.set( Math.PI / 3 , 0, 0)
              this.setDoorAnimation(targetDoor,{x: Math.PI / 3})
              targetDoor.userData.isOpen = true
            }else {
              // 以开门 => 关门
              // targetDoor.rotation.set( 0 , 0, 0)
              this.setDoorAnimation(targetDoor,{x: 0})
              targetDoor.userData.isOpen = false
            }
          
            // console.log('将来点击时触发这里的代码')
          })





        }
      })
    })
     

    }
    // 车门动画 
    setDoorAnimation(mesh,obj) {
      gsap.to(mesh.rotation, {
        x: obj.x,  // 动画x轴
        duration:1, // 动画持续时间1秒
        ease:'powerl.in' //先慢 后快
      })
    }

    // 摄像机和轨道控制器的动画
     // 摄像机和轨道控制器动画
     setCameraAnimation(dataObj) {
      gsap.to(this.camera.position, {
        ...dataObj.camera,
        duration: 1,
        ease: 'power1.in'
      })
      gsap.to(this.controls.target, {
        ...dataObj.controls,
        duration: 1,
        ease: 'power1.in'
      })
    }
   
}