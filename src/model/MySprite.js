/*
 * @FilePath: MySprite.js
 * @Author: 杜芬
 * @Date: 2024-04-20 12:42:40
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-04-20 16:45:23
 * Copyright: 2024 xxxTech CO.,LTD. All Rights Reserved.
 * @Descripttion: 页面
 */
// 专门产生精灵物体类
import * as THREE from 'three'
// 专门产生精灵物体类
export class MySprite {
  constructor({ name, url, position, scale }) {
    const texture = (new THREE.TextureLoader()).load(url)
    const spriteMaterial = new THREE.SpriteMaterial({ map: texture })
    const sprite = new THREE.Sprite(spriteMaterial)
    sprite.position.set(...position)
    sprite.scale.set(...scale)
    sprite.name = name

    // 直接返回精灵物体对象（而非 new 创建的空白对象）
    return sprite
  }
}

