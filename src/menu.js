/*
 * @FilePath: menu.js
 * @Author: 杜芬
 * @Date: 2024-04-21 10:34:02
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-04-22 15:37:08
 * Copyright: 2024 xxxTech CO.,LTD. All Rights Reserved.
 * @Descripttion: 页面
 */
// 汽车菜单--DOM绑定事件
import {  EventBus  } from '@/utils/EventBus'
// 颜色选择
const colorDivList = document.querySelectorAll('.col_group>div')
colorDivList.forEach(el => {
  el.addEventListener('click', () => {
    const colorStr = el.dataset.col

    // 
    EventBus.getInstance().emit('changeCarColor',colorStr)
  })
})


// 贴膜切换
const coatDivList = document.querySelectorAll('.coat_group>div')
coatDivList.forEach(el => {
  el.addEventListener('click' , () => {
    const coatName = el.dataset.co
    // 贴膜名字=》事件总线 =》Car 类
    // console.log(123,coatName)
    EventBus.getInstance().emit('changeCarCoat', coatName)
    EventBus.getInstance().emit('celPrice') // 计算汽车最新价格
  })
})

// 场景切换
const sceneDivList = document.querySelectorAll('.scene_group>div')
sceneDivList.forEach(el => {
 
  el.addEventListener('click', () => {
    const sceneName = el.dataset.poi
// console.log(sceneName,11111)
    // 场景名称 =》 事件总线 =》 Sky 类
    EventBus.getInstance().emit('changeSky', sceneName)
  })
})


// 切换视角
const lookDivList = document.querySelectorAll('.look_group>div')
lookDivList.forEach(el => {
  el.addEventListener('click' ,() => {
    const viewName = el.dataset.po
     // 视角名称 =》 事件总线 =》car 类
    //  console.log(el.dataset,11111)
     EventBus.getInstance().emit('changeCarAngleView', viewName)
  })
})
