/*
 * @FilePath: main.js
 * @Author: 杜芬
 * @Date: 2024-04-19 12:56:21
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-04-21 10:45:22
 * Copyright: 2024 xxxTech CO.,LTD. All Rights Reserved.
 * @Descripttion: 页面
 */
import {camera} from  '@/entry'
import { ClickHandler } from '@/utils/ClickHandler'
import { EventBus  } from '@/utils/EventBus'

import '@/menu.js'

ClickHandler.getInstance().init(camera)
// 订阅事件
// 传入自定义事件的名字 和自定义事件的函数
// EventBus.getInstance().on('changePrice', (oldPrice,newPrice) => {
//   console.log(oldPrice,newPrice)
// })


// 触发 
// EventBus.getInstance().emit('changePrice', 100, 200)