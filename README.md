# online-car

#### 介绍
使用技术three+vite+js 在线选车项目,包含贴图，UV贴图，场景，摄像机，内外场景切换，玻璃透视，贴膜，在线切换汽车颜色。曝光度，场景光，平行光，几何图形。封装EventBus类，各种材质，小物体更改。
项目下载git clone https://gitee.com/dufen123/online-car
npm i
npm run dev

#### 软件架构
软件架构说明


#### 安装教程

1.  项目下载git clone https://gitee.com/dufen123/online-car
2.  安装依赖包 npm i
4.  拉取最新代码 git pull origin master
3.  项目运行 npm run dev

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
